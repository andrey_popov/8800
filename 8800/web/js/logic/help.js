$(document).ready(function()
{
	$("body").hide();
    init();
	this.title = "Помощь";
	authorization();
	initAddMoneyForm();
	initAuthorizationForm();
	initRegistrationForm();
    initRecoverPasswordForm();
	loadMoney();
	initLogout();
    loadContent();
    initDinamics();
	$("body").show();
});
function loadContent()
{
    var url_string = base_app_url+"/Catalogue?cmd=get_content";

    $.ajax({
        async:false,
        cache:false,
        url: url_string,
        context: document.body,
        dataType: "xml",
        type: 'POST',
        success: function(xml){
            var code = $("root > code",xml).text();
            //alert("auth code: "+code);
            if(code=="100000")// success
            {

                var data = $("root > data",xml).text();
                $("#main").html(data);
            }
            else
            {
                alert("!!");
                alert("Server error");
            }
        }
    });
}
function initDinamics()
{
    //<p class="opener" id="korm">Как???</p>
    //<p class="korm" style="display: none;">А вот так!</p>
    $(".opener").click(function(){
        var id = $(this).attr("id");
        var answer = $("."+id);
        if($(answer).css("display")=="none")// Must open
        {
            $(answer).css("display","block");
        }
        else// Must close
        {
            $(answer).css("display","none");
        }
    });
}



