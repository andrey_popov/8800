package ftwo.newmethod.structure;

import ftwo.library.access.User;
import ftwo.library.database.DBRecord;
import java.util.Date;


public class NewMethodUser extends User
{
	private long LastCardActivationTime;
	private long ActivationCardTimeout = 5000;

	public NewMethodUser(DBRecord rec)
	{
		super(rec);
		LastCardActivationTime = 0;
	}

	public boolean canActivateCard()
	{
		boolean res = new Date().getTime()-LastCardActivationTime>ActivationCardTimeout;
		LastCardActivationTime = new Date().getTime();
		return res;
	}
}
