package ftwo.newmethod.servlets;

import com.smartxls.RangeStyle;
import com.smartxls.WorkBook;
import ftwo.library.access.User;
import ftwo.library.database.DBRecord;
import ftwo.library.database.DBTable;
import ftwo.library.json.JSONProcessor;
import ftwo.library.xml.XMLProcessor;
import ftwo.newmethod.database.DBProcessor;
import ftwo.newmethod.ozon.APIProcessor;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminServlet extends BaseServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = null;
        String cmd = request.getParameter("cmd");
        try
        {
            if(cmd == null)
            {
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", "/admin/admin_auth.html");
                out.close();
                return;
            }
            User u = AuthenticationServlet.adminAuthorization(request);
			if(u.getIntValue("role_id")==0)// Admin
			{
                if(cmd.equalsIgnoreCase("set_content"))
                {
                    out = response.getWriter();
                    String html = request.getParameter("html_data");
                    dbProc().setContent("help",html);
                    XMLProcessor proc = new XMLProcessor();
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_content"))
                {
                    out = response.getWriter();
                    String[] names = new String[1];
                    names[0] = "help";
                    DBTable t = dbProc().getContent(names);
                    String html = "";
                    if(t!=null&&t.size()>0)
                    {
                        html = t.get(0).getStringValue("value");
                    }
                    XMLProcessor proc = new XMLProcessor();
                    proc.addNode("root", "data",html);
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("check_user_info"))
                {
                    out = response.getWriter();
                    String user_id = request.getParameter("user_id");
                    JSONProcessor proc = ozonProc().getUserInfo(user_id);
                    out.print(generateResponseXML(REQUEST_PROCESSED_SUCCESSFULLY, cmd, proc.getStructure()));
                }
                else if(cmd.equalsIgnoreCase("get_users"))
                {
                    out = response.getWriter();
                    int page = Integer.valueOf(request.getParameter("page"));
                    int page_size = Integer.valueOf(request.getParameter("page_size"));

                    String login = request.getParameter("login");
                    DBTable dbt = ((DBProcessor)BaseServlet.dbProc()).getUsers(page, page_size,login);
                    XMLProcessor proc = dbt.toXMLProcessor();
                    int total_pages = (int)((dbt.size())/page_size)+1;
                    proc.addNode("root","total_pages",total_pages);
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_cards"))
                {
                    out = response.getWriter();
                    int page = Integer.valueOf(request.getParameter("page"));
                    int page_size = Integer.valueOf(request.getParameter("page_size"));
                    int total_pages = (int)(((Long)dbProc().getTable("SELECT count(*) AS size FROM cards").get(0).getValue("size"))/page_size)+1;
                    XMLProcessor proc = ((DBProcessor)BaseServlet.dbProc()).getCards(page, page_size).toXMLProcessor();
                    proc.addNode("root","total_pages",total_pages);
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_orders"))
                {
                    out = response.getWriter();
                    int page = Integer.valueOf(request.getParameter("page"));
                    int page_size = Integer.valueOf(request.getParameter("page_size"));
                    int total_pages = (int)(((Long)dbProc().getTable("SELECT count(*) AS size FROM orders").get(0).getValue("size"))/page_size)+1;
                    XMLProcessor proc = ((DBProcessor)BaseServlet.dbProc()).getOrders(page, page_size).toXMLProcessor();
                    proc.addNode("root","total_pages",total_pages);
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("check_order"))
                {
                    out = response.getWriter();
                    String order_number = request.getParameter("order_number");
                    String user_id = request.getParameter("user_id");
                    XMLProcessor proc = ozonProc().getOrder(user_id, order_number).toXMLProcessor();
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_statistics"))
                {
                    out = response.getWriter();
                    String from = request.getParameter("from");
                    String to = request.getParameter("to");
                    DBRecord registrations = ((DBProcessor)BaseServlet.dbProc()).getActivities(5,from,to,null);
                    DBRecord orders = ((DBProcessor)BaseServlet.dbProc()).getActivities(4,from,to,null);
                    DBRecord cards = ((DBProcessor)BaseServlet.dbProc()).getActivities(2,from,to,null);

                    Object reg_count = registrations.getValue("count");
                    Object orders_sum = orders.getValue("value");
                    Object orders_count = orders.getValue("count");
                    Object cards_sum = cards.getValue("value");
                    Object cards_count = cards.getValue("count");
                    XMLProcessor proc = new XMLProcessor();
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    proc.addNode("root", "reg_count",reg_count);
                    proc.addNode("root", "orders_sum",orders_sum);
                    proc.addNode("root", "orders_count",orders_count);
                    proc.addNode("root", "cards_sum",cards_sum);
                    proc.addNode("root", "cards_count",cards_count);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_lists"))
                {
                    out = response.getWriter();
                    XMLProcessor proc = ((DBProcessor)BaseServlet.dbProc()).getPhoneLists().toXMLProcessor();
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("generate_report"))
                {
                    response.setContentType("application/xls");

                    response.addHeader("Content-Disposition", "attachment; filename=report.xls");
                    String login = request.getParameter("login");
                    String report_type = request.getParameter("type");
                    String from = request.getParameter("from");
                    String to = request.getParameter("to");
                    if(report_type.equalsIgnoreCase("users"))
                    {
                        generateSingleReport(from,to,response.getOutputStream());
                    }
                    else
                    {
                        generateSystemReport(from,to,response.getOutputStream());
                    }

                }
                else if(cmd.equalsIgnoreCase("refresh_number_list"))
                {
                    out = response.getWriter();
                    String path_prefix = request.getServletContext().getRealPath("");
                    File f = new File(path_prefix+"\\black_list\\list.csv");
                    if(f.exists())
                    {
                        BufferedReader bl = new BufferedReader( new FileReader(path_prefix+"\\black_list\\list.csv"));
                        dbProc().executeDelete("phone_list","is_black=TRUE");
                        String strLine = "";
                        while( (strLine = bl.readLine()) != null)
                        {
                            HashMap<String,Object> pars = new HashMap<String, Object>();
                            pars.put("phone",strLine.trim().replace("(","").replace(")","").replace("-",""));
                            pars.put("is_black",true);
                            try
                            {
                                dbProc().executeInsert("phone_list",pars);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    f = new File(path_prefix+"\\white_list\\list.csv");
                    if(f.exists())
                    {
                        BufferedReader wl = new BufferedReader( new FileReader(path_prefix+"\\white_list\\list.csv"));
                        dbProc().executeDelete("phone_list","is_black=FALSE");
                        String strLine = "";
                        while( (strLine = wl.readLine()) != null)
                        {
                            HashMap<String,Object> pars = new HashMap<String, Object>();
                            pars.put("phone",strLine.trim().replace("(","").replace(")","").replace("-",""));
                            pars.put("is_black",false);
                            try
                            {
                                dbProc().executeInsert("phone_list",pars);
                            }
                            catch(Exception ex)
                            {

                            }
                        }
                    }

                    XMLProcessor proc = ((DBProcessor)BaseServlet.dbProc()).getPhoneLists().toXMLProcessor();
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
	        }
            else
            {
                out = response.getWriter();
                out.print(generateResponseXML(BASE_ADMIN_AUTH_FAILED, null));
            }
        }
        catch (Exception ex)
        {
            if(out==null)
            {
                out = response.getWriter();
            }
            out.print(generateResponseExceptionXML(BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            if(out==null)
            {
                out = response.getWriter();
            }
            out.close();
        }
    } 
    public void generateSingleReport(String from,String to,OutputStream stream) throws Exception
    {
        DBTable tab = ((DBProcessor)BaseServlet.dbProc()).getUsersActivities(from,to,true);
        DBTable tab1 = ((DBProcessor)BaseServlet.dbProc()).getUsersActivities(from,to,false);

        WorkBook wb = new WorkBook();
        String real_path = this.getServletContext().getRealPath("");
        wb.read(real_path+"/templates/users_report.xls");

        wb.setText(0,0,"Отчет по пользователям С "+from+" По "+to);
        int row_num = 5;

        wb.setText(4,0,"№");
        wb.setText(4,1,"Пользователь");
        wb.setText(4,2,"Возраст");
        wb.setText(4,3,"Пол");
        wb.setText(4,4,"Дата регистрации");
        wb.setText(4,5,"Карт акт-но.");
        wb.setText(4,6,"Заказы, руб.");
        wb.setText(4,7,"Количество, шт.");

        Long epic_cards = 0l;
        Long epic_orders_price = 0l;
        Long epic_orders_count = 0l;
        for(int i=0; i<tab.size(); i++)
        {
            DBRecord rec = tab.get(i);
            Integer id = rec.getIntValue("id");
            String gender = rec.getStringValue("gender");
            String age = rec.getStringValue("age");
            String login = rec.getStringValue("login");
            Date created = (Date)rec.getValue("created");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String cr = formatter.format(created);
            Long cards_activated = rec.getLongValue("cards_activated");
            if(cards_activated==null)
            {
                cards_activated=0l;
            }
            Long total_orders_price = rec.getLongValue("total_orders_price");
            if(total_orders_price==null)
            {
                total_orders_price=0l;
            }
            Long total_orders = rec.getLongValue("total_orders");
            if(total_orders==null)
            {
                total_orders=0l;
            }
            epic_cards+=cards_activated;
            epic_orders_price+=total_orders_price;
            epic_orders_count+=total_orders;
            wb.setText(i+row_num,0,id.toString());
            wb.setText(i+row_num,1,login.toString());
            wb.setText(i+row_num,2,age);
            wb.setText(i+row_num,3,gender.toString());
            wb.setText(i+row_num,4,cr.toString());
            wb.setText(i+row_num,5,cards_activated.toString());
            wb.setText(i+row_num,6,total_orders_price.toString());
            wb.setText(i+row_num,7,total_orders.toString());
        }
        row_num+=tab.size()+2;
        wb.setText(row_num,0," ИТОГО: ");
        wb.setText(row_num,3,epic_cards.toString());
        wb.setText(row_num,4,epic_orders_price.toString());
        wb.setText(row_num,5,epic_orders_count.toString());
        RangeStyle rangeStyle = wb.getRangeStyle(row_num, 0, row_num, 6);
        rangeStyle.setFontBold(true);
        wb.setRangeStyle(rangeStyle, row_num, 0, row_num, 6);

        // Else users...
        wb.setText(row_num+3,0,"Пользователи общего списка");
        rangeStyle = wb.getRangeStyle(row_num+3, 0, row_num+3, 1);
        rangeStyle.setFontBold(true);
        wb.setRangeStyle(rangeStyle, row_num+3, 0, row_num+3, 1);
        wb.setText(row_num+5,0,"№");
        wb.setText(row_num+5,1,"Пользователь");
        wb.setText(row_num+5,2,"Возраст");
        wb.setText(row_num+5,3,"Пол");
        wb.setText(row_num+5,4,"Дата регистрации");
        wb.setText(row_num+5,5,"Карт акт-но.");
        wb.setText(row_num+5,6,"Заказы, руб.");
        wb.setText(row_num+5,7,"Количество, шт.");

        row_num+=6;
        epic_cards = 0l;
        epic_orders_price = 0l;
        epic_orders_count = 0l;
        for(int i=0; i<tab1.size(); i++)
        {
            DBRecord rec = tab1.get(i);
            Integer id = rec.getIntValue("id");
            String gender = rec.getStringValue("gender");
            String age = rec.getStringValue("age");
            String login = rec.getStringValue("login");
            Date created = (Date)rec.getValue("created");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String cr = formatter.format(created);
            Long cards_activated = rec.getLongValue("cards_activated");
            if(cards_activated==null)
            {
                cards_activated=0l;
            }
            Long total_orders_price = rec.getLongValue("total_orders_price");
            if(total_orders_price==null)
            {
                total_orders_price=0l;
            }
            Long total_orders = rec.getLongValue("total_orders");
            if(total_orders==null)
            {
                total_orders=0l;
            }
            epic_cards+=cards_activated;
            epic_orders_price+=total_orders_price;
            epic_orders_count+=total_orders;
            wb.setText(i+row_num,0,id.toString());
            wb.setText(i+row_num,1,login.toString());
            wb.setText(i+row_num,2,age);
            wb.setText(i+row_num,3,gender.toString());
            wb.setText(i+row_num,4,cr.toString());
            wb.setText(i+row_num,5,cards_activated.toString());
            wb.setText(i+row_num,6,total_orders_price.toString());
            wb.setText(i+row_num,7,total_orders.toString());
        }
        row_num+=tab1.size()+2;
        wb.setText(row_num,0," ИТОГО: ");
        wb.setText(row_num,3,epic_cards.toString());
        wb.setText(row_num,4,epic_orders_price.toString());
        wb.setText(row_num,5,epic_orders_count.toString());
        rangeStyle = wb.getRangeStyle(row_num, 0, row_num, 6);
        rangeStyle.setFontBold(true);
        wb.setRangeStyle(rangeStyle, row_num, 0, row_num, 6);

        wb.write(stream);
    }
    public void generateSystemReport(String from,String to,OutputStream stream) throws Exception
    {
        DBRecord registrations = ((DBProcessor)BaseServlet.dbProc()).getActivities(5,from,to,null);
        DBRecord orders = ((DBProcessor)BaseServlet.dbProc()).getActivities(4,from,to,null);
        DBRecord cards = ((DBProcessor)BaseServlet.dbProc()).getActivities(2,from,to,null);

        DBRecord registrations_white = ((DBProcessor)BaseServlet.dbProc()).getActivities(5,from,to,true);
        DBRecord orders_white = ((DBProcessor)BaseServlet.dbProc()).getActivities(4,from,to,true);


        Object reg_count = registrations.getValue("count");
        Object orders_sum = orders.getValue("value");
        Object orders_count = orders.getValue("count");

        Object reg_count_white = registrations_white.getValue("count");
        Object orders_sum_white = orders_white.getValue("value");
        Object orders_count_white = orders_white.getValue("count");

        Object cards_sum = cards.getValue("value");
        Object cards_count = cards.getValue("count");

        WorkBook wb = new WorkBook();
        String real_path = this.getServletContext().getRealPath("");
        wb.read(real_path+"/templates/single_report.xls");
        wb.setText(0,1,"С "+from);
        wb.setText(0,2,"По "+to);

        wb.setText(3,1,reg_count.toString());// Registrations for period...
        wb.setText(4,1,((Long)((Long)reg_count-(Long)reg_count_white)).toString());// Registrations total...
        wb.setText(5,1,reg_count_white.toString());// Registrations white...

        wb.setText(8,1,cards_sum.toString());// Cards summ
        wb.setText(9,1,cards_count.toString());// Cards count

        wb.setText(12,1,orders_sum.toString());// Orders sum
        wb.setText(13,1,orders_count.toString());// Orders count
        wb.setText(14,1,((Long)((Long)orders_sum-(Long)orders_sum_white)).toString());// Orders lumpen sum
        wb.setText(15,1,((Long)((Long)orders_count-(Long)orders_count_white)).toString());// Orders lumpen count
        wb.setText(16,1,orders_sum_white.toString());// Orders white sum
        wb.setText(17,1,orders_count_white.toString());// Orders white count

        wb.write(stream);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
