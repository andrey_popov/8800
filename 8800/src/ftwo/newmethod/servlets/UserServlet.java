package ftwo.newmethod.servlets;

import ftwo.library.access.User;
import ftwo.library.database.DBTable;
import ftwo.library.json.JSONProcessor;
import ftwo.library.settings.Settings;
import ftwo.library.sms.SMSMessage;
import ftwo.library.xml.XMLProcessor;
import ftwo.newmethod.database.DBProcessor;
import ftwo.newmethod.structure.NewMethodUser;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserServlet extends BaseServlet
{
	public static Integer CARD_ACTIVATION_TIMEOUT = -11;
	public static Integer CARD_ACTIVATION_SUCCESS = 1;
	public static Integer CARD_ACTIVATION_FAILURE = -1;
	
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String cmd = request.getParameter("cmd");
        try
        {
            NewMethodUser user = (NewMethodUser)(AuthenticationServlet.authorization(request));

            if(user!=null)
            {
                if(cmd == null)
                {
                    out.print(generateResponseXML(BASE_COMMAND_UNDEFINED, cmd, null));
                }
                else if(cmd.equalsIgnoreCase("activate_card"))
                {
                    String code = request.getParameter("code");
                    if(user.canActivateCard())
                    {
                        String user_id = String.valueOf(user.getID());
                        JSONProcessor result = ozonProc().activateCard(user_id, code);
                        XMLProcessor proc = result.toXMLProcessor();

                        if(result.getDoubleValue("Status")==2)// OK
                        {
                            int value = result.getIntValue("DCode.DiscountValue");
                            ((DBProcessor)BaseServlet.dbProc()).updateCard(code, true, user.getID(), value);
                            proc.addNode("root", "code",CARD_ACTIVATION_SUCCESS);
                            out.print(proc.toXMLString());
                            String cardActivationMessage = Settings.getStringSetting("card_activation_message");
                            cardActivationMessage = cardActivationMessage.replace("<value>", String.valueOf(value));
                            String phone_number = user.getStringValue("login");
                            BaseServlet.smsProc().sendSMS(new SMSMessage(phone_number, cardActivationMessage, "BonusKarta"));
                        }
                        else
                        {
                            proc.addNode("root", "code",CARD_ACTIVATION_FAILURE);
                            out.print(proc.toXMLString());
                        }
                    }
                    else
                    {
                        out.print(generateResponseXML(CARD_ACTIVATION_TIMEOUT, cmd, null));
                    }
                }
                else if(cmd.equalsIgnoreCase("get_content"))
                {
                    String[] names = new String[1];
                    names[0] = "help";
                    DBTable t = dbProc().getContent(names);
                    String html = "";
                    if(t!=null&&t.size()>0)
                    {
                        html = t.get(0).getStringValue("value");
                    }
                    XMLProcessor proc = new XMLProcessor();
                    proc.addNode("root", "data",html);
                    proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                    out.print(proc.toXMLString());
                }
                else if(cmd.equalsIgnoreCase("get_money_information"))
                {
                    String user_id = String.valueOf(user.getID());
                    JSONProcessor proc = ozonProc().getUserInfo(user_id);
                    out.print(generateResponseXML(REQUEST_PROCESSED_SUCCESSFULLY, cmd, proc.getStructure()));
                }
                else if(cmd.equalsIgnoreCase("get_orders"))
                {
                    String user_id = String.valueOf(user.getID());
                    JSONProcessor proc = ozonProc().getOrders(user_id);
                    out.print(generateResponseXML(REQUEST_PROCESSED_SUCCESSFULLY, cmd, proc.getStructure()));
                }
                else if(cmd.equalsIgnoreCase("get_order"))
                {
                    String user_id = String.valueOf(user.getID());
                    String order_number = request.getParameter("order_number");
                    JSONProcessor proc = ozonProc().getOrder(user_id,order_number);
                    out.print(generateResponseXML(REQUEST_PROCESSED_SUCCESSFULLY, cmd, proc.getStructure()));
                }
                else
                {
                    out.print(generateResponseXML(BASE_UNKNOWN_COMMAND, cmd, null));
                }
            }
            else
            {
                out.print(generateResponseXML(AUTHORIZATION_FAILED, cmd, null));
                out.close();
                return;
            }
        }
        catch (Exception ex)
        {
            out.print(generateResponseExceptionXML(BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
