package ftwo.newmethod.servlets;

import ftwo.library.json.JSONProcessor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends BaseServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
		//AreaGroupCollection.item_1.AreaAddressCollection.AddressCollection.item_1.AreaId
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String cmd = request.getParameter("cmd");
        try
        {
  			System.out.println(" ============================================================ ");
			System.out.println(" ============================================================ ");
			JSONProcessor cartAddResp = ozonProc().cartAdd("49", "8615487:1");
			cartAddResp = ozonProc().cartAdd("49", "8615487:1");
			cartAddResp = ozonProc().cartAdd("49", "8615487:1");
            JSONProcessor startOrderResp = ozonProc().startOrder("49");
			String guid = (String)startOrderResp.getValue("OrderGuid");
			
			JSONProcessor getAvalableRegionsResp = ozonProc().getAvalableRegions("49", guid);
			JSONProcessor getDeliveryVariantsResp = ozonProc().getDeliveryVariants("49", guid, "0", "2");
			JSONProcessor getPaymentVariantsResp = ozonProc().getPaymentVariants("49", guid, "0", "2","211");
			JSONProcessor getOrderParametersForCollectionResp = ozonProc().getOrderParametersForCollection("49", guid, "0", "2","211","4","");
			JSONProcessor saveOrderResp = ozonProc().saveOrder("49", guid, "0", "2","211"
					,"4"
					,"982450"
					,"Russia"
					,"Moscow_area"
					,"Some_district"
					,"Zheleznodorozhny"
					,"Dima"
					,"Novaya_43_119"
					,"0"
					,"9255713588"
					,"36152"
					,"Dima"
					,"S"
					,"Fedorovich");
			String addr_id = String.valueOf(((Double)saveOrderResp.getValue("NewAddressId")).intValue());
			JSONProcessor checkOrderResp = ozonProc().checkOrder("49", guid, addr_id,"2", "211", "1", "4", "0", "false");
			JSONProcessor summaryOrderResp = ozonProc().summaryOrder("49", guid, addr_id, "211", "4", "1", "0", "false");
			JSONProcessor createOrderResp = ozonProc().createOrder("49", guid, addr_id, "211", "4", "1", "0","9255713588","comment","dfedorovich85@gmail.com","Dima","false","36152");
			JSONProcessor getOrdersResp = ozonProc().getOrders("49");
			out.print(getOrdersResp.toXMLProcessor().toXMLString());
        }
        catch (Exception ex)
        {
            out.print(generateResponseExceptionXML(BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
