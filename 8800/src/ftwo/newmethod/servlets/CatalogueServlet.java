package ftwo.newmethod.servlets;

import ftwo.library.database.DBTable;
import ftwo.library.json.JSONProcessor;
import ftwo.library.xml.XMLProcessor;
import ftwo.newmethod.ozon.APIProcessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CatalogueServlet extends BaseServlet
{
    private HashMap<String,XMLProcessor> cash = new HashMap<String, XMLProcessor>();
    private long last_cash_update = new Date().getTime();

    private boolean needReload()
    {
        long gone = new Date().getTime()-last_cash_update;
        if(gone>21600000)
        {
            last_cash_update = new Date().getTime();
            return true;
        }
        return false;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String cmd = request.getParameter("cmd");
        try
        {
            if(cmd == null)
            {
                out.print(generateResponseXML(BASE_COMMAND_UNDEFINED, cmd, null));
            }
            else if(cmd.equalsIgnoreCase("get_catalogue_structure"))
            {
                JSONProcessor result = BaseServlet.ozonProc().getCatalogueStructure();

                out.print(generateResponseXML(REQUEST_PROCESSED_SUCCESSFULLY,cmd, result.getStructure()));
            }
            else if(cmd.equalsIgnoreCase("get_catalogue_items"))
            {
                String catalogue_id = request.getParameter("catalogue_id");
                String items_on_page = request.getParameter("items_on_page");
                String page_number = request.getParameter("page_number");
                XMLProcessor xmlP = null;
                String url = catalogue_id+"_"+items_on_page+"_"+page_number;
                boolean need_reload = needReload();
                if(cash.get(url)==null||need_reload)
                {
                    
                    JSONProcessor result = ozonProc().getCatalogueItems(catalogue_id, "istName", items_on_page, page_number);
                    int total_items = result.getIntValue("TotalItems");
                    int items_on_p = Integer.valueOf(items_on_page);
                    int total_pages = (total_items/items_on_p)+1;
                    xmlP = result.toXMLProcessor();
                    xmlP.addNode("root", "total_pages", total_pages);
                    cash.put(url, xmlP);
                }
                else
                {
                    xmlP = cash.get(url);
                    xmlP.addNode("root", "total_pages", xmlP.getValue("root.total_pages"));
                    cash.put(url, xmlP);
                }
                String result_string = xmlP.toXMLString();
                out.println(result_string);
            }
            else if(cmd.equalsIgnoreCase("get_content"))
            {
                String[] names = new String[1];
                names[0] = "help";
                DBTable t = dbProc().getContent(names);
                String html = "";
                if(t!=null&&t.size()>0)
                {
                    html = t.get(0).getStringValue("value");
                }
                XMLProcessor proc = new XMLProcessor();
                proc.addNode("root", "data",html);
                proc.addNode("root", "code",REQUEST_PROCESSED_SUCCESSFULLY);
                out.print(proc.toXMLString());
            }
            else if(cmd.equalsIgnoreCase("get_item"))
            {
                String item_id = request.getParameter("item_id");
                JSONProcessor result = ozonProc().getItem(item_id);
                String result_string = result.toXMLProcessor().toXMLString();
                out.println(result_string);
            }
        }
        catch (Exception ex)
        {
            out.print(generateResponseExceptionXML(BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
