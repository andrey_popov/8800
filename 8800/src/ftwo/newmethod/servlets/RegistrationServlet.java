package ftwo.newmethod.servlets;

import ftwo.library.helper.Helper;
import ftwo.library.json.JSONProcessor;
import ftwo.library.sms.SMSMessage;
import ftwo.newmethod.database.DBProcessor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationServlet extends BaseServlet
{
    public static int REGISTRATION_FAILED_PHONE_IN_BLACK_LIST = -1;
    public static int REGISTRATION_FAILED_PHONE_ALREADY_EXISTS = -2;
    public static int REGISTRATION_FAILED_OZON_ERROR = -3;
    public static int REGISTRATION_SUCCESSFUL = 1;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String cmd = request.getParameter("cmd");
        try
        {
            if(cmd == null)
            {
                out.print(generateResponseXML(BASE_COMMAND_UNDEFINED, cmd, null));
            }
            else if(cmd.equalsIgnoreCase("create_registration"))
            {
                String login = request.getParameter("login");
                String pass = request.getParameter("password");
                String gender = request.getParameter("gender");
                String age = request.getParameter("age");
                //String pass = Helper.generatePassword(5);
                String email = "newmethod"+login+"@ruru.ru";
                int phone_status = ((DBProcessor)BaseServlet.dbProc()).checkPhoneNumber(login);

                if(phone_status==((DBProcessor)BaseServlet.dbProc()).USER_PHONE_ALREADY_EXISTS)
                {
                    out.print(generateResponseXML(REGISTRATION_FAILED_PHONE_ALREADY_EXISTS, cmd, null));
                }
                else if(phone_status==((DBProcessor)BaseServlet.dbProc()).USER_PHONE_IN_BLACK_LIST)
                {
                    out.print(generateResponseXML(REGISTRATION_FAILED_PHONE_IN_BLACK_LIST, cmd, null));
                }
                else
                {
                    HashMap<String,Object> pars = new HashMap<String, Object>();
                    pars.put("login", login);
                    pars.put("email", email);
                    pars.put("password", pass);
                    pars.put("gender", gender);
                    pars.put("age", age);
                    int user_id = dbProc().registerUser(pars);
                    JSONProcessor response_proc = ozonProc().registerUser(email, pass, String.valueOf(user_id), "", "");

                    if((double)response_proc.getDoubleValue("Status") == 1)
                    {
                        ((DBProcessor)BaseServlet.dbProc()).deleteUser(user_id);
                        String r = generateResponseXML(REGISTRATION_FAILED_OZON_ERROR, cmd, "error", "GlobalOzonError");
                        out.print(r);
                    }
                    else
                    {
                        out.print(generateResponseXML(REGISTRATION_SUCCESSFUL, cmd, "password",pass));
                        BaseServlet.smsProc().sendSMS(new SMSMessage(login, "Поздравляем, Вы зарегистрированы! Для входа зайдите на 88005551073.ru, введите свой мобильный телефон и пароль ...", "BonusKarta"));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            out.print(generateResponseExceptionXML(BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            out.close();
        }
    } 
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
