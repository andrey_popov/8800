package ftwo.newmethod.servlets;

import ftwo.library.access.Session;
import ftwo.library.access.User;
import ftwo.library.database.DBRecord;
import ftwo.library.sms.SMSMessage;
import ftwo.newmethod.database.DBProcessor;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationServlet extends ftwo.library.web.AuthenticationServlet
{
    public static Integer AUTHENTICATION_NO_LOGIN = -1;
    public static Integer AUTHENTICATION_NO_PASSWORD = -2;
    public static Integer AUTHENTICATION_WRONG_LOGIN_OR_PASSWORD = -3;
    public static Integer AUTHORIZATION_FAILED = -4;
    public static Integer LOGOUT_FAILED = -5;
    public static Integer RECOVER_NO_SUCH_USER = -6;
    public static Integer RECOVER_CANT_RECOVER = -7;

    public static Integer AUTHENTICATION_SUCCESSFUL = 1;
    public static Integer AUTHORIZATION_SUCCESSFUL = 2;
    public static Integer LOGOUT_SUCCESSFUL = 3;


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String cmd = request.getParameter("cmd");
        try
        {
            if(cmd == null)
            {
                out.print(BaseServlet.generateResponseXML(BaseServlet.BASE_COMMAND_UNDEFINED, cmd, null));
            }
            else if(cmd.equalsIgnoreCase("authentication"))
            {
                String login = request.getParameter("login");
                String password = request.getParameter("password");
                String admin = request.getParameter("admin");
                if(login == null)
                {
                    out.print(BaseServlet.generateResponseXML(AUTHENTICATION_NO_LOGIN, cmd, null));
                    out.close();
                    return;
                }
                else if(password == null)
                {
                    out.print(BaseServlet.generateResponseXML(AUTHENTICATION_NO_PASSWORD, cmd, null));
                    out.close();
                    return;
                }
                else
                {
                    String session_id = authentication(login, password);
                    if(session_id==null)
                    {
                        out.print(BaseServlet.generateResponseXML(AUTHENTICATION_WRONG_LOGIN_OR_PASSWORD, cmd, "session_id",session_id));
                        out.close();
                        return;
                    }
                    else
                    {
                        if(admin!=null)
                        {
                            int role_id = BaseServlet.sessProc().getSession(session_id).getUser().getIntValue("role_id");
                            if(role_id==0)
                            {
                                String expires = getSessionExpires();
                                String cookie = "admin_session_id="+session_id+"; path=/; Expires="+expires;
                                response.setHeader("Set-Cookie", cookie);
                                out.print(BaseServlet.generateResponseXML(AUTHENTICATION_SUCCESSFUL, cmd, "session_id",session_id));
                                out.close();
                            }
                            else
                            {
                                out.print(BaseServlet.generateResponseXML(AUTHENTICATION_WRONG_LOGIN_OR_PASSWORD, cmd, "session_id",session_id));
                                out.close();
                                return;
                            }
                        }
                        else
                        {
                            String expires = getSessionExpires();
                            String cookie = "session_id="+session_id+"; path=/; Expires="+expires;
                            response.setHeader("Set-Cookie", cookie);
                            out.print(BaseServlet.generateResponseXML(AUTHENTICATION_SUCCESSFUL, cmd, "session_id",session_id));
                            out.close();
                        }
                        return;
                    }
                }
            }
            else if(cmd.equalsIgnoreCase("authorization"))
            {
                User user = null;
                if(request.getParameter("admin")!=null)
                {
                    user = adminAuthorization(request);
                }
                else
                {
                    user = authorization(request);
                }

                if(user!=null)
                {
                    out.print(BaseServlet.generateResponseXML(AUTHORIZATION_SUCCESSFUL, cmd,"login",user.getStringValue("login")));
                    out.close();
                    return;
                }
                else
                {
                    out.print(BaseServlet.generateResponseXML(AUTHORIZATION_FAILED, cmd, null));
                    out.close();
                    return;
                }
            }
            else if(cmd.equalsIgnoreCase("recover"))
            {

                String phone = request.getParameter("phone");
                boolean can_recover = ((DBProcessor)BaseServlet.dbProc()).tryRecoverPassword(phone);
                if(can_recover)
                {
                    DBRecord rec = BaseServlet.dbProc().getRecord("SELECT password FROM users WHERE login='"+phone.replace("'", "")+"'");
                    if(rec!=null)
                    {
                        String pass = rec.getStringValue("password");
                        BaseServlet.smsProc().sendSMS(new SMSMessage(phone, "Здравствуйте! Ваш пароль на 88005551073.ru "+pass, "BonusKarta"));
                        out.print(BaseServlet.generateResponseXML(BaseServlet.REQUEST_PROCESSED_SUCCESSFULLY, cmd));
                        out.close();
                        return;
                    }
                    else
                    {
                        out.print(BaseServlet.generateResponseXML(RECOVER_NO_SUCH_USER, cmd, null));
                        out.close();
                        return;
                    }
                }
                else
                {
                    out.print(BaseServlet.generateResponseXML(RECOVER_CANT_RECOVER, cmd, null));
                    out.close();
                    return;
                }
            }
            else if(cmd.equalsIgnoreCase("logout"))
            {
                User user = authorization(request);

                if(user!=null)
                {
                    Session s = getSession(request);
                    Boolean was_session = false;
                    if(s!=null)
                    {
                        was_session = BaseServlet.sessProc().removeSession(s.getKey());
                    }
                    out.print(BaseServlet.generateResponseXML(LOGOUT_SUCCESSFUL, cmd,"was_session",was_session.toString()));
                    out.close();
                    return;
                }
                else
                {
                    out.print(BaseServlet.generateResponseXML(LOGOUT_FAILED, cmd, null));
                    out.close();
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            out.print(BaseServlet.generateResponseExceptionXML(BaseServlet.BASE_UNKNOWN_COMMAND, cmd, ex));
        }
        finally
        {
            out.close();
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
