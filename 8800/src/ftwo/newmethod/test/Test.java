package ftwo.newmethod.test;

import ftwo.library.http.HTTPClient;
import ftwo.library.json.JSONProcessor;
import ftwo.newmethod.ozon.APIProcessor;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import sun.net.www.http.HttpClient;


public class Test
{
    public static void main(String[] args)
    {
	long start = new Date().getTime();
        try
        {
	    APIProcessor proc = new APIProcessor("newmethod", "dsdERfdhgD34i23df", "https://ows.ozon.ru/PartnerService");
	    JSONProcessor result = proc.getCatalogueStructure();
	    HashMap<String,Object> map = result.getStructure();

	    String result_string = result.toXMLProcessor().toXMLString();
	    System.out.println(result_string);
	    System.out.println(result.getStructure());
        }
        catch(Exception ex)
        {
	    System.out.println(ex.toString());
        }
	finally
	{
	    System.out.println("Total time: "+(new Date().getTime()-start));
	}
    }
}
