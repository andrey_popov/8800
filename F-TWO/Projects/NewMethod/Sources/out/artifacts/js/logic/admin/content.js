var html="";
$(document).ready(function()
{
    $("body").hide();
    $(".search").hide();
    init();
    if(admin_authorization()!=true)
    {
        window.location="../admin/admin_auth.html";
    }
    else
    {
        loadContentEditors();

        $("body").show();
    }
});
function loadContentEditors()
{
    tmce = tinyMCE.init({
        // General options
        mode : "textareas",
        //elements : "content",

        theme : "advanced",
        plugins : "lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        width: "500",
        // Theme options
        theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        onchange_callback : "editContent",
        oninit : "loadContent",
        // Skin options
        skin : "o2k7",
        skin_variant : "silver"

    });
    $("#save_butt").click(function(){
        saveContent();
    });
}
function loadContent()
{
    var url_string = base_app_url+"/Admin?cmd=get_content";

    $.ajax({
        async:false,
        cache:false,
        url: url_string,
        context: document.body,
        dataType: "xml",
        type: 'POST',
        success: function(xml){
            var code = $("root > code",xml).text();
            //alert("auth code: "+code);
            if(code=="100000")// success
            {

                var data = $("root > data",xml).text();
                var ed = tinyMCE.get("content");
                ed.setContent(data);

                var tab = $(".mceEditor > table");
                $(".mceEditor > table").css("width","900px");
            }
            else
            {
                alert("Server error");
            }
        }
    });
}
function saveContent()
{
    var url_string = base_app_url+"/Admin?cmd=set_content";

    $.ajax({
        async:false,
        cache:false,
        url: url_string,
        context: document.body,
        data: {
            'foo': 'bar',
            'html_data': html // <-- the $ sign in the parameter name seems unusual, I would avoid it
        },
        dataTypeString:"xml",
        type: 'POST',
        success: function(xml){
            var code = $("root > code",xml).text();
            //alert("auth code: "+code);
            if(code=="100000")// success
            {
                alert("Успешно сохранено...");
            }
            else
            {
                alert("Server error");
            }
        }
    });
}
function editContent(inst)
{
    html = inst.getBody().innerHTML
    return;
}